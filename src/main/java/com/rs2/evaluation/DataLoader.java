package com.rs2.evaluation;

import com.rs2.evaluation.model.Product;
import com.rs2.evaluation.model.User;
import com.rs2.evaluation.repository.ProductRepository;
import com.rs2.evaluation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DataLoader implements ApplicationRunner {

    private UserRepository userRepository;
    private ProductRepository productRepository;

    @Autowired
    public DataLoader(UserRepository userRepository, ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    public void run(ApplicationArguments args) {
        if(!userRepository.findAll().iterator().hasNext()){
            userRepository.save(new User("Mark"));
            userRepository.save(new User("Willian"));
            userRepository.save(new User("John"));
        }

        if(!productRepository.findAll().iterator().hasNext()){
            productRepository.save(new Product("Moby Dick", Product.TYPE_BOOKS));
            productRepository.save(new Product("The Incredibles", Product.TYPE_BOOKS));
            productRepository.save(new Product("Super Mario", Product.TYPE_GAMES));
            productRepository.save(new Product("Sonic", Product.TYPE_GAMES));
            productRepository.save(new Product("We are the World", Product.TYPE_MUSIC));
            productRepository.save(new Product("Abbey Road", Product.TYPE_MUSIC));
        }
    }
}