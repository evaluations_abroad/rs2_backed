package com.rs2.evaluation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class EvaluationProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvaluationProjectApplication.class, args);
    }

}
