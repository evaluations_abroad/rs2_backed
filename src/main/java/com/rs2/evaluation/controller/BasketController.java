package com.rs2.evaluation.controller;

import com.rs2.evaluation.model.Basket;
import com.rs2.evaluation.model.Product;
import com.rs2.evaluation.model.User;
import com.rs2.evaluation.repository.BasketRepository;
import com.rs2.evaluation.repository.ProductRepository;
import com.rs2.evaluation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path="/basket")
public class BasketController {

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/clear")
    @Transactional
    public String clearBasket(
            @RequestParam(name="userId", required=true) Integer userId,
            Model model,
            final RedirectAttributes redirectAttributes){

        basketRepository.deleteAllByUser(userRepository.findById(userId).get());

        redirectAttributes.addFlashAttribute("message", "Your basket is empty!");

        return "redirect:/product/find";
    }

    @PostMapping("/add")
    public String addToBasket(
            @RequestParam(name="userId", required=true) Integer userId,
            @RequestParam(name="productsId", required=true) List<Integer> productsId,
            Model model,
            final RedirectAttributes redirectAttributes){

        User loggedUser = userRepository.findById(userId).get();

        List<String> errorProducts = new ArrayList<String>();

        productsId.forEach((productId)->{
            Product product = productRepository.findById(productId).get();

            if(basketRepository.findByProduct(product).size() == 0){
                basketRepository.save(new Basket(loggedUser, product));
            }else{
                errorProducts.add(product.getName());
            }
        });

        if(errorProducts.size() > 0){
            redirectAttributes.addFlashAttribute("errorMessage", "The following products are already on basket: " + errorProducts);
        }else{
            redirectAttributes.addFlashAttribute("message", "Products added to basket");
        }

        return "redirect:/product/find";
    }
}
