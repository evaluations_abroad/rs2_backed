package com.rs2.evaluation.controller;

import com.rs2.evaluation.model.Basket;
import com.rs2.evaluation.model.User;
import com.rs2.evaluation.repository.BasketRepository;
import com.rs2.evaluation.repository.ProductRepository;
import com.rs2.evaluation.repository.ProductRepositoryCustom;
import com.rs2.evaluation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import org.springframework.ui.Model;

@Controller
@RequestMapping(path="/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BasketRepository basketRepository;

    @GetMapping("/find")
    public String product(
            @RequestParam(name="name", required=false) String name,
            @RequestParam(name="type", required=false) String type,
            Model model,
            @ModelAttribute("errorMessage") final Object errorMessage,
            @ModelAttribute("message") final Object message){

        if((name == null || name.equals("")) && (type == null || type.equals(""))) {
            model.addAttribute("products", productRepository.findAll());
        }else{
            model.addAttribute("products", productRepository.findProducts(name, type));
        }

        User loggedUser = userRepository.findById(1).get();
        List<Basket> userBasket = basketRepository.findAllByUser(loggedUser);

        model.addAttribute("name", name != null ? name: "");
        model.addAttribute("type", type != null ? type : "");
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute( "userBasket", userBasket);

        if(errorMessage != null && errorMessage instanceof String && !errorMessage.equals("")){
            model.addAttribute("errorMessage", errorMessage);
        }else{
            model.addAttribute("errorMessage", "");
        }

        if(message != null && message instanceof String && !message.equals("")){
            model.addAttribute("message", message);
        }else{
            model.addAttribute("message", "");
        }

        return "product";
    }
}
