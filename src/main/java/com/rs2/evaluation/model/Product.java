package com.rs2.evaluation.model;

import javax.persistence.*;

@Entity
public class Product {

    public Product(){}

    public Product(String name, String type){
        this.name  = name;
        this.type = type;
    }

    public static final String TYPE_BOOKS = "books";
    public static final String TYPE_MUSIC = "music";
    public static final String TYPE_GAMES = "games";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(length = 32, name = "Name")
    private String name;

    @Column(length = 8, name = "Type")
    private String type;

    @ManyToOne
    @JoinColumn(name="UserID", nullable=true)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Product{" +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}