package com.rs2.evaluation.model;

import javax.persistence.*;

@Entity
public class User {

    public User(){}

    public User(String userName){
        this.userName  = userName;
    }

    @Id
    @Column(name = "UserID")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer userID;

    @Column(length = 40, name = "UserName")
    private String userName;

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}