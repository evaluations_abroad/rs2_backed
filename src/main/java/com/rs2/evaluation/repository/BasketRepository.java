package com.rs2.evaluation.repository;

import com.rs2.evaluation.model.Basket;
import com.rs2.evaluation.model.Product;
import com.rs2.evaluation.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BasketRepository extends CrudRepository<Basket, Integer> {
    List<Basket> findAllByUser(User user);
    List<Basket> findByProduct(Product product);
    void deleteAllByUser(User user);
}

