package com.rs2.evaluation.repository;

import com.rs2.evaluation.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer>, ProductRepositoryCustom {
    List<Product> findProducts(String name, String type);
}

