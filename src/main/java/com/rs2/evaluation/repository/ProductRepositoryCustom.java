package com.rs2.evaluation.repository;

import com.rs2.evaluation.model.Product;

import java.util.List;

public interface ProductRepositoryCustom {
    List<Product> findProducts(String name, String type) ;
}
