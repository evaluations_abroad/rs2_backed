package com.rs2.evaluation.repository;

import com.rs2.evaluation.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Set;


@Repository
public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Product> findProducts(String name, String type) {

        String query = "select p from Product p where 1 = 1 ";

        if(name != null && !name.equals("")){
            query += " and p.name like '%" + name + "%'";
        }

        if(type != null && !type.equals("")){
            query += " and p.type = '" + type + "'";
        }

        return entityManager.createQuery(query)
                .getResultList();
    }
}

