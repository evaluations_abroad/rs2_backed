package com.rs2.evaluation.repository;

import com.rs2.evaluation.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}

